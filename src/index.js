export default function slugger(...str) {
    return str.join('-').split(" ").join("-");
}

